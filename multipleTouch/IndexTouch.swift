//
//  IndexTouch.swift
//  multipleTouch
//
//  Created by Nguyễn Đình Vương on 11/27/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import Foundation

class IndexTouch{
    var x:Float=0
    var y:Float=0
    
    init(x:Float,y:Float) {
        self.x=x
        self.y=y
    }
}
