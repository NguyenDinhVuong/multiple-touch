//
//  ViewController.swift
//  multipleTouch
//
//  Created by Nguyễn Đình Vương on 11/26/18.
//  Copyright © 2018 Nguyễn Đình Vương. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isMultipleTouchEnabled = true
    }
    @IBAction func btnprint(_ sender: Any) {
        var t : Float = 0
        listD=[]
        for i in 0..<x.count{
            let x1:Float=x[i]
            let y1:Float=y[i]
            for j in i+1..<x.count{
                let x2:Float=x[j]
                let y2:Float=y[j]
                let powx = pow(x2-x1, 2)
                let powy = pow(y2-y1, 2)
                t=sqrt(powx+powy)
                listD.append(t)
            }
        }
        
        sortBubble(listSort: listD)
    }
    
    func sortBubble(listSort : [Float]) -> [Float]{
        var sort : Float
        var list : [Float] = listSort
        for i in 0..<list.count-1{
            for j in 0..<list.count-1-i{
                var sort : Float
                if (list[j] > list[j+1]){
                    sort = list[j];
                    list[j] = list[j+1];
                    list[j+1] = sort;
                }
            }
        }
        
        return list
    }
    
    var fingers = [String?](repeating: nil, count:5)
    var listTouch : [IndexTouch]?
    var x : [Float] = []
    var y : [Float] = []
    var listD : [Float]=[]
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //lay vi tri khi cham vao man hinh
        super.touchesBegan(touches, with: event)
        x=[]
        y=[]
        for touch in touches{
            fingers=[String?](repeating: nil, count:5)
            let point = touch.location(in: self.view)
            x.append(Float(point.x))
            y.append(Float(point.y))
//            for (index,finger)  in fingers.enumerated() {
//                if finger == nil {
//                    fingers[index] = String(format: "%p", touch)
//                    print("finger \(index+1): x=\(point.x) , y=\(point.y)")
//                    break
//                }
//            }
        }
    }
    
    ////lay vi tri khi duy chuyen
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesMoved(touches, with: event)
//        for touch in touches {
//            let point = touch.location(in: self.view)
//            for (index,finger) in fingers.enumerated() {
//                if let finger = finger, finger == String(format: "%p", touch) {
//                    print("finger \(index+1): x=\(point.x) , y=\(point.y)")
//                    break
//                }
//            }
//        }
//
//    }
//
    ////xoa vi tri khi ngac tay khoi man hinh
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesEnded(touches, with: event)
//        for touch in touches {
//            for (index,finger) in fingers.enumerated() {
//                if let finger = finger, finger == String(format: "%p", touch) {
//                    fingers[index] = nil
//                    break
//                }
//            }
//        }
//    }
}

